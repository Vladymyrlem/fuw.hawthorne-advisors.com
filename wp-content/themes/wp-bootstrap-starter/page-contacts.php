<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>


<!-- Contact Area Start -->
    <div scroll-spy="true" id="contact-us" class="chipsofic-content-block">
        <div class="container">
          <div class="row">
             <div class="col-lg-12">
                   <div class="section-title text-center">
                        <h1>Contact Chipsofic</h1>
                        <div class="chipsofic-subtitle">Contact us for further query and application</div>
                   </div>
               </div>
            </div>
           <div class="row">
                <div class="col-lg-6">
                   <div class="contact-info-block">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis. </p>
                        <ul>
                            <li><i class="fa fa-envelope"></i> <a href="mailto:text@example.com"> text@example.com</a></li>
                            <li><i class="fa fa-phone"></i> <a href="tel:123-456-789"> (123) 456-789</a></li>
                            <li><i class="fa fa-map-marker"></i> 123 ABC Road, USA</li>
                        </ul>
                    </div>
               </div>

                <div class="col-lg-6">
                    <div class="contact-form wow fadeInRight">
                        <form action="" method="post">
                           <div class="row">
                               <div class="col-lg-6">
                                   <input id="client-name" name="client-name" type="text" placeholder="Name" required="required">
                               </div>
                               <div class="col-lg-6">
                                   <input id="client-email" name="client-email" type="email" placeholder="Email" required="required">
                               </div>
                               <div class="col-lg-12">
                                   <textarea name="client-message" id="client-message" cols="30" rows="10" placeholder="Your Message"></textarea>
                                   <p><button class="btn chipsofic-fullwidth-btn" type="submit">Submit</button></p>
                               </div>
                           </div>

                            <div class="row">
                                <div class="col-lg-12">
                                   <div class="form-response"></div>
                               </div>
                           </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-12">
                    <!-- Google Map Area Start -->
                    <div scroll-spy="true" id="our-location">
                        <div class="map">
                            <div class="gmap">
                                <div id="map1"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Google Map Area End -->
                </div>

            </div>
        </div>
    </div>
    <!-- Contact Area End -->

<?php
//get_sidebar();
get_footer();
