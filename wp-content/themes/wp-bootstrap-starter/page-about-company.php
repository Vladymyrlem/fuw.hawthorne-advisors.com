<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>


<!-- Welcome Area Start -->
    <div scroll-spy="true" id="about-us" class="chipsofic-content-block">
        <div class="container">
            <div class="about-us-section">
               <div class="row">
                    <div class="col-lg-6">
                        <h1><span>We are</span> Chipsofic</h1>
                        <h2>Your Friendly Neighbourhood Chips</h2>
                        <p>Diam nonummy nibh, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio.</p>
                        <div class="row">
                            <div class="col-md-6">
                               <h3><i class="fa fa-ban" aria-hidden="true"></i><span>No</span> Preservatives</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A autem doloremque ea eum eveniet quas.</p>
                            </div>
                            <div class="col-md-6">
                               <h3><i class="fa fa-heartbeat" aria-hidden="true"></i><span>Health</span> Friendly</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A autem doloremque ea eum eveniet quas.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                               <h3><i class="fa fa-money" aria-hidden="true"></i><span>Low</span> Cost</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A autem doloremque ea eum eveniet quas.</p>
                            </div>
                            <div class="col-md-6">
                               <h3><i class="fa fa-commenting" aria-hidden="true"></i><span>24/7</span> Support</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A autem doloremque ea eum eveniet quas.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 wow fadeInRight">
                        <img src=" <?php echo get_stylesheet_directory_uri();?>/assets/img/about-chips.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Welcome Area End -->
			<?php
			while ( have_posts() ) : the_post();

//				get_template_part( 'template-parts/content', 'page' );


			endwhile; // End of the loop.
			?>



<?php
//get_sidebar();
get_footer();
