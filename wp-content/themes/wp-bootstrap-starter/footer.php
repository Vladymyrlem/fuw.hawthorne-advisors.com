<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<!-- Footer Top Area Start -->
                    <div class="footer-top-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <div class="footer-widget">
                                    <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
                                                                                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
                                                                                            <img src="<?php echo esc_url(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                                                                                    <?php endif; ?>
                                        <!--<h3 class="footer-logo"><img src="assets/img/footer-logo.png" alt="Footer Logo"></h3>-->
                                        <?php
                                                                                    wp_nav_menu(array(
                                                                                    'theme_location'    => 'foot-menu',
                                                                                    'container'       => 'div',
                                                                                    'container_id'    => 'footer-main-nav',
                                                                                    'container_class' => 'footer-menu navbar-collapse justify-content-center',
                                                                                    'menu_id'         => false,
                                                                                    'menu_class'      => '',
                                                                                    'depth'           => 3,
                                                                                    ));

                                        ?>
                                      <p><a href="mailto:text@example.com"><i class="fa fa-envelope"></i> text@example.com</a> | <a href="tel:123-456-789"><i class="fa fa-phone"></i> (123) 456-789</a> | <i class="fa fa-map-marker"></i> 123 ABC Road, USA</p>

                                        <div class="footer-social">
                                            <ul>
                                                <li><a href="http://facebook.com" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                                <li><a href="http://twitter.com" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                                                <li><a href="http://linkedin.com" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                                                <li><a href="http://pinterest.com" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Top Area End -->

                    <!-- Footer Copyright Area Start -->
                    <div class="footer-copyright-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    &copy; Copyright © 2020  Fayzizoda Foods | Bсе права защищены
                                </div>
                            </div>
                            <a data-scroll href="#top" class="go-top">Top <i class="fa fa-long-arrow-up"></i></a>
                        </div>
                    </div>
                    <!-- Footer Copyright Area End -->
		<div class="container pt-3 pb-3">
            <div class="site-info">
                &copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
                <span class="sep"> | </span>
                <a class="credits" href="oqila.uz" target="_blank" title="OQILA" alt="OQILA">
                <?php echo esc_html__('Разработка сайта - OQILA-logo(link to oqila.uz','wp-bootstrap-starter'); ?>
                </a>
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
