<?php
/**
 * The template for displaying front page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

        <!-- Choose Our Chips Area Start -->
        <div scroll-spy="true" id="our-products" class="chipsofic-content-block section-gray">
            <div class="container">
              <div class="row">
                 <div class="col-lg-12">
                       <div class="section-title text-center">
                            <h1>Choose Your Flavor</h1>
                            <div class="chipsofic-subtitle">Choose any of our chips which better suits your needs</div>
                       </div>
                   </div>
                </div>

               <div class="choose-our-chips-content-area text-center wow fadeInUp">
                    <div class="row">
                        <div class="col-md-6 col-lg-3 single-chips-item">
                          <img src="assets/img/chips-01.png" alt="">
                           <h3>Chipsofic Potato</h3>
                           <div class="pricing"><span>Price:</span> $40.00</div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            <a class="btn chipsofic-small-btn" data-scroll href="#purchase-now">Order Now</a>
                        </div>
                        <div class="col-md-6 col-lg-3 single-chips-item">
                           <img src="assets/img/chips-02.png" alt="">
                           <h3>Chipsofic Masala</h3>
                           <div class="pricing"><span>Price:</span> $50.00</div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            <a class="btn chipsofic-small-btn" data-scroll href="#purchase-now">Order Now</a>
                        </div>
                        <div class="col-md-6 col-lg-3 single-chips-item">
                           <img src="assets/img/chips-03.png" alt="">
                           <h3>Chipsofic Ring</h3>
                           <div class="pricing"><span>Price:</span> $30.00</div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            <a class="btn chipsofic-small-btn" data-scroll href="#purchase-now">Order Now</a>
                        </div>
                        <div class="col-md-6 col-lg-3 single-chips-item">
                           <img src="assets/img/chips-04.png" alt="">
                           <h3>Chipsofic Pasta</h3>
                           <div class="pricing"><span>Price:</span> $70.00</div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            <a class="btn chipsofic-small-btn" data-scroll href="#purchase-now">Order Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Choose Our Chips Area End -->

        <!-- Our Process Area Start -->
        <div scroll-spy="true" class="chipsofic-content-block process-block">
            <div class="container">
              <div class="row">
                 <div class="col-lg-12">
                       <div class="section-title text-center">
                            <a onclick="changeVideo('ctvlUvN6wSE')"><i class="fa fa-play-circle"></i></a>
                            <h1>Check our Process</h1>
                            <div class="chipsofic-subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt</div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
        <!-- Video Popup -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-label="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="iframeYoutube" class="embed-responsive-item" width="816" height="459" src="https://www.youtube.com/embed/ctvlUvN6wSE"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Our Process Area End -->

        <!-- Gallery Area Start -->
        <div scroll-spy="true" id="pic-gallery" class="chipsofic-content-block gallery-block">
            <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                           <div class="section-title text-center">
                                <h1>Picture Gallery</h1>
                                <div class="chipsofic-subtitle">Check our aweomse product images</div>
                           </div>
                       </div>
                    </div>
            </div>
            <div class="gallery-area">
                <div class="container-fluid">
                    <div class="row wow fadeInUp">
                        <!-- Gallery 1st Row Start -->
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 01" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-01.png">
                            <i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-01.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 02" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-02.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-02.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 03" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-03.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-03.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 04" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-04.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-04.png" alt=""></a>
                        </div>
                        <!-- Gallery 1st Row End -->

                        <!-- Gallery 2nd Row Start -->
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 05" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-05.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-05.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 06" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-06.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-06.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 07" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-07.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-07.png" alt=""></a>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <a title="Lorem Ipsum 08" class="gallery-lightbox" href="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-08.png"><i class="fa fa-plus"></i> <img src="<?php echo get_stylesheet_directory_uri();?>/img/gallery-img-08.png" alt=""></a>
                        </div>
                        <!-- Gallery 2nd Row End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Gallery Area End -->

        <!-- Our Progress Area Start -->
        <div scroll-spy="true" class="chipsofic-content-block counter-block">
            <div class="container">
               <div class="row">
                 <div class="col-lg-12">
                       <div class="section-title text-center">
                            <h1>Our Progress</h1>
                            <div class="chipsofic-subtitle">Check our progress over the years</div>
                       </div>
                   </div>
                </div>

               <div class="chipsofic-counter-area text-center">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                           <h2><span class="counter">37,020</span></h2>
                            <p>Yearly Customer</p>
                        </div>
                        <div class="col-md-6 col-lg-3">
                           <h2><span class="counter">5,01,212</span></h2>
                            <p>Sell Per Year</p>
                        </div>
                        <div class="col-md-6 col-lg-3">
                           <h2><span class="counter">9,210</span></h2>
                            <p>Staff</p>
                        </div>
                        <div class="col-md-6 col-lg-3">
                           <h2><span class="counter">5,001</span></h2>
                            <p>Showroom</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Progress Area End -->
        <div scroll-spy="true" id="contacts" class="chipsofic-content-block contacts-block">
                <div class="container">
                   <div class="row">
                        <div class="col-lg-6">
                           <div class="contacts">
                                   <p>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Etiam pellentesque tortor id dui accumsan vestibulum. Vestibulum vitae molestie est. Vivamus non volutpat erat. Nulla massa lacus, hendrerit sed convallis non, cursus ut orci. Vestibulum eget dapibus enim. Nunc vestibulum vehicula neque, a aliquet nibh suscipit ac.
                                    </p>
                                    <div class="contacts-list" style="">
                                    <a href="mailto:text@example.com"><i class="fa fa-envelope"></i> text@example.com</a>
                                    <a href="tel:123-456-789"><i class="fa fa-phone"></i> (123) 456-789</a>
                                    <span><i class="fa fa-map-marker"></i> 123 ABC Road, USA</span>
                                    </div>
                            </div>
                        </div>

                       <div class="col-lg-6">
                           <div class="contacts text-center">
                              <form action="YOUR_MAILCHIMP_URL" method="post" target="_blank">
                                  <div class="row">
                                       <div class="col-lg-6"><input class="form-control" id="first-name" type="text" name="YOURNAME" placeholder="Имя*"></div>
                                       <div class="col-lg-6"><input class="form-control" id="email" type="email" name="EMAIL" placeholder="E-mail*"></div>
                                       <div class="col-lg-12"><textarea class="form-control" id="msg" name="msg" placeholder="Текст сообщения"></textarea></div>
                                       <div class="col-lg-12"><button class="btn chipsofic-form-btn" type="submit">Subscribe Now</button></div>
                                   </div>
                                </form>
                            </div>
                       </div>
                   </div>
                </div>
            </div>
<?php
//get_sidebar();
get_footer();
