<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 

    // WordPress 5.2 wp_body_open implementation
    if ( function_exists( 'wp_body_open' ) ) {
        wp_body_open();
    } else {
        do_action( 'wp_body_open' );
    }

?>

<div id="page" class="site">
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>

    <!-- Main Header Area Start -->
    <div class="header-area">
       <div class="cbp-af-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="logo">
                            <a href="#top">
                            <div class="navbar-brand">
                                                <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
                                                    <a href="<?php echo esc_url( home_url( '/' )); ?>">
                                                        <img src="<?php echo esc_url(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                    </a>
                                                <?php else : ?>
                                                    <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                                                <?php endif; ?>

                                            </div>
                                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                                                <span class="navbar-toggler-icon"></span>
                                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                            <?php
                                            wp_nav_menu(array(
                                            'theme_location'    => 'primary',
                                            'container'       => 'div',
                                            'container_id'    => 'main-nav',
                                            'container_class' => 'main-menu navbar-collapse justify-content-end',
                                            'menu_id'         => false,
                                            'menu_class'      => '',
                                            'depth'           => 3,
                                            'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                            'walker'          => new wp_bootstrap_navwalker()
                                            ));
                                            ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mobile Menu Area Start -->
            <div class="container"><div class="responsive-menu-wrap"></div></div>
        <!-- Mobile Menu Area End -->

        <div class="container">
            <div class="row">
                <div class="top-area text-center">
                    <h1>Chipsofic: Leader of Potato Chips</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                    <a data-scroll href="#about-us" class="btn">Learn More</a>
                </div>
            </div>
        </div>

    </div>
    <!-- Main Header Area End -->
	<div id="content" class="site-content">
		<div class="">
			<div class="">
                <?php endif; ?>
