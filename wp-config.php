<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'N+[xAWBAN~~%b`o*> ts0?pf$r?g+*`{k*)VAMt+sHqH?]WF:&HQ)mf2T_@ZE`oB' );
define( 'SECURE_AUTH_KEY',  'B?a/m6%#wvqK%4rLxV<GN~XAuD*LL(Q.J,CgixMxtbM>ejbKq 7WBvz/NYyp;l[h' );
define( 'LOGGED_IN_KEY',    'wyA<mH$8#{I/R VFH_ B;yD!x[&Qj swB[i9~(C`umS`|KHr_XVDr8NH5(&T|6l*' );
define( 'NONCE_KEY',        'bEA!B}Sci{ j$m{VE>4Q~{#o.Kt%Y9*molD}nZQ/k=CJd[Ac/w-Xj+PD9]yy)#!7' );
define( 'AUTH_SALT',        '~zqojw|!ApB%IsmSK8kPj]N(8093NOX&H5&s!^*8Xj$T}-M/km2pUCE<)*iYQcwY' );
define( 'SECURE_AUTH_SALT', '<Dm=h!Uy$U^-[rQX0e-{Xt- Fwr2^V.WiDL8i][aYW:%1&4E4-G4,hxnY^~Xk#S%' );
define( 'LOGGED_IN_SALT',   'eE%wQ8![f0eo>wa&IUEFjD~i^2`.WtTqS`hB7kRZmR^f/^XiM-jV;xK8x!A}~MqJ' );
define( 'NONCE_SALT',       'iEh1?UKf8z[KFu+ZraokwqfnUzZ)ZjMaHWLg{NKl.E[xIRcO>x=l5i;Uzv0d.[Z(' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
